package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
				String id = request.getParameter("id");


				// 確認用：idをコンソールに出力
				System.out.println(id);

				// idを引数にして、idに紐づくユーザ情報を取ってくる
				UserDao userDao = new UserDao();
				User userUpdate = userDao.findById(id);

				// ユーザ情報をリクエストスコープにセットしてjspにフォワード
				request.setAttribute("loginId",userUpdate.getLoginId());
				request.setAttribute("userName",userUpdate.getName());
				request.setAttribute("birth",userUpdate.getBirthDate());

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String pass = request.getParameter("pass");
		String passC = request.getParameter("passC");
		String userName = request.getParameter("user");
		String birth = request.getParameter("birth");

		UserDao userDao = new UserDao();

		if((!(pass.equals(passC)))||userName.equals("")||birth.equals("")) {
			request.setAttribute("passErr","入力された内容は正しくありません。");
			// ユーザ情報をリクエストスコープにセットしてjspにフォワード
			request.setAttribute("loginId",loginId);
			request.setAttribute("userName",userName);
			request.setAttribute("birth",birth);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return ;
		}else if(pass.equals("") && passC.equals("")) {
			userDao.UpdateUser2(loginId,userName,birth);
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
			return;
		}

		//UpdateUser
		userDao.UpdateUser(loginId,pass,userName,birth);


		// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("UserListServlet");


	}

}
