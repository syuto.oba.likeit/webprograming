package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserNewRegister
 */
@WebServlet("/UserNewRegister")
public class UserNewRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserNewRegister() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String pass = request.getParameter("pass");
		String passC = request.getParameter("passC");
		String userName = request.getParameter("userName");
		String birth = request.getParameter("birth");
		//else if
		UserDao userDao = new UserDao();
		String loginId2 =userDao.findByLoginId(loginId);

		if((!(pass.equals(passC)))||loginId.equals("")||pass.equals("")||passC.equals("")||userName.equals("")||birth.equals("")||loginId.equals(loginId2)) {
			request.setAttribute("checkErr", "入力した内容は正しくありません");
			// ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
						dispatcher.forward(request, response);
						return;
		}

		//NewUser
		userDao.NewUser(loginId,pass,userName,birth);


		// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("UserListServlet");


	}
}


