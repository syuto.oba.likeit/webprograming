<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Update title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<c:if test="${passErr != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${passErr}
		</div>
</c:if>

	<div class="user row col-4">
		<p>${userInfo.name} さん
		<p><a href="LogoutServlet">ログアウト</a></p>
	</div>


 	<h1 align="center">ユーザー情報更新</h1>

<form action="UserUpdate" method="post">
<input class="ml-5" name="loginId" type="hidden" value="${loginId}" style="width:200;">
<div class="col-4 mx-auto">
 		<p>ログインID　　　　${loginId}

		</p>
</div>

<div class="col-4 mx-auto">
 		<p>パスワード
		<input class="ml-5" name="pass" type="password" style="width:200;">
		</p>

		<p>パスワード(確認)
 		<input class="ml-1" name="passC" type="password" style="width:200;">
		</p>

		<p>ユーザー名
 		<input class="ml-5" name="user" type="text"  value="${userName}" style="width:200;">
		</p>

		<p class="ml-3">生年月日
 		<input class="ml-5" name="birth" type="date" style="width:200;" value="${birth}">
		</p>
</div>
		<p align="center">
		<input type="submit" value="更新">
		</p>
</form>
		<p class="col-2 ml-5" align="left"><a href="UserListServlet">戻る</a></p>

</body>
</html>