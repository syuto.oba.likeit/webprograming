<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user row col-4">
		<p >${userInfo.name} さん
		<p><a href="LogoutServlet">ログアウト</a></p>
	</div>

	<h1 align="center">ユーザー削除確認</h1>

	<p align="center">${UserDelete.loginId}<br>
	を本当に削除してもよろしいでしょうか。
	</p>
<form action="UserListServlet" method="get">
<div class="col-2 mx-auto">
<input type="submit" value="キャンセル">
</div>
</form>

<form action="UserDelete" method="post">
<input class="ml-5" name="id" type="hidden" value="${UserDelete.id}" style="width:200;">
<div class="col-2 mx-auto">
<input type="submit" value="OK">
</div>
</form>


</body>
</html>