<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<c:if user="${checkErr != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${checkErr}
		</div>
</c:if>
	<div class="user row col-4">
		<p>${userInfo.name} さん
		<p><a href="LoginServlet">ログアウト</a></p>
	</div>


 	<h1 align="center">ユーザー新規登録</h1>

<form action="UserNewRegister" method="post">
	<div class="col-4 mx-auto">
 		<p>ログインID
 		<input class="ml-5" name="loginId" type="text" style="width:200;">
		</p>

 		<p>パスワード
		<input class="ml-5" name="pass" type="password" style="width:200;">
		</p>

		<p >パスワード(確認)
 		<input class="ml-1" name="passC"type="password" style="width:200;">
		</p>

		<p>ユーザー名
 		<input class="ml-5" name="userName" type="text" style="width:200;">
		</p>

		<p  class="ml-3">生年月日
 		<input class="ml-5" name="birth" type="date" style="width:200;">
		</p>
</div>
		<p align="center">
		<input type="submit" value="登録">
		</p>
</form>

		<p class="ml-5" align="left"><a href="UserListServlet">戻る</a></p>

</body>
</html>