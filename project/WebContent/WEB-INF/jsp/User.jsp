<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>User</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div class="user row col-4" align="right">
			<p>${userInfo.name} さん
			<p>
				<a href="LogoutServlet">ログアウト</a>
			</p>
	</div>


	<h1 align="center">ユーザー覧</h1>
	<div class="user row col-4 mx-right">
		<a href="UserNewRegister">新規登録</a>
	</div>
<form action="UserListServlet" method="post">
	<div class="col-4 mx-auto">
		<p>
			ログインID <input type="text" name="logId" size="width:20;">
		</p>

		<p>
			ユーザー名 <input type="text" name="userName" size="width:20;">
		</p>

		<p>
				生年月日 <input type="date" name="birthm" size="10" >
				~ <input type="date" name="birthM" size="10" >
		</p>
	</div>

	<p align="right">
		<input class="mr-4" type="submit" value="検索">
	</p>
</form>
	<div class="col-6 mx-auto">
		<hr color="#000000">

		<table border="4">
		<thead>
			<tr bgcolor="#cccccc">
				<th>ログインID</th>
				<th>ユーザー名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
			</thead>

			<tbody>

			 <c:forEach var="user" items="${userList}" >
			  <tr>
				<td>${user.loginId}</td>
				<td>${user.name}</td>
				<td>${user.birthDate}</td>
				<td>
				<c:if test="${userInfo.loginId =='admin'}">
					   <a class="btn btn-primary"  href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdate?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDelete?id=${user.id}">削除</a>
                </c:if>
                <c:if test="${userInfo.loginId !='admin'}">
					   <a class="btn btn-primary"  href="UserDetailServlet?id=${user.id}">詳細</a>
                       <c:if test="${userInfo.loginId ==user.loginId}">
                       <a class="btn btn-success" href="UserUpdate?id=${user.id}">更新</a>
                       </c:if>
                </c:if>
                </td>
			  </tr>
			 </c:forEach>
			</tbody>
		</table>
	</div>



</body>
</html>