<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="user row col-4">
<p>${userInfo.name} さん
	<p><a href="LogoutServlet">ログアウト</a></p>
</div>

 	<h1 align="center">ユーザー情報詳細参照</h1>
<div class="col-4 mx-auto">
 		<p>ログインID
 		${user.loginId}
		</p>

 		<p>ユーザー名
		${user.name}
		</p>

		<p>生年月日
 		${user.birthDate}
		</p>

		<p>登録日時
 		${user.createDate}
		</p>

		<p>更新日時
 		${user.updateDate}
		</p>
</div>
		<p class="ml-5" align="left"><a href="UserListServlet">戻る</a></p>

</body>
</html>