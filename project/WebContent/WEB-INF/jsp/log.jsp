<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

<c:if user="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
</c:if>



	<br>
	<form action="LoginServlet" method="post">
		<h1 align="center">ログイン画面</h1>
		<br>

		<div class="col-4 mx-auto">
			<p>
				ログインID <input class="ml-5" name="loginId" type="text" style="width: 200;">
			</p>

			<p>
				パスワード <input class="ml-5" name="password" type="password" style="width: 200;">
			</p>
		</div>
		<br>
		<p align="center">
			<input type="submit" class="btn btn-outline-primary" value="ログイン">
	</form>
</body>
</html>